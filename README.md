# dolock.me

Web which should teach people to lock their computers while they are away and improve security.

Available at https://dolock.me

License: MIT, see `LICENSE` file.

Authors:
- [Radek Lát](https://lat.sk)
- [Martin Vician](https://vician.net)
